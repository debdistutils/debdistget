<?php

# Copyright (C) 2024 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# https://github.com/git-lfs/git-lfs/blob/main/docs/api/batch.md

$in = json_decode(file_get_contents('php://input'), true);

if ($in['operation'] != "download") {
    error_log ("operation!=download: " . $in['operation']);
    http_response_code(404);
    die();
}

error_log ("IN " . json_encode ($in));

$out['transfer'] = "basic";

foreach ($in['objects'] as &$object) {
    $oid = $object['oid'];
    if (!preg_match('/^[0-9a-f]{64}$/', $oid)) {
        error_log ("bad oid: " . $oid);
        http_response_code(404);
        die();
    }
    $i = substr($oid, 0, 2);
    $j = substr($oid, 2, 2);

    $object['authenticated'] = true;
    $download['href'] = "http://" . $_SERVER['SERVER_NAME'] . "/farm/$i/$j/$oid";
    $actions['download'] = $download;
    $object['actions'] = $actions;
}

$out['objects'] = $in['objects'];
$out['hash_algo'] = "sha256";

$outstr = json_encode($out);
error_log ("OUT " . $outstr);

header('Content-Type: application/vnd.git-lfs+json');
print json_encode($out);
?>
