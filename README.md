# debdistget

## What Is This?

Debdistget retrieve index files from a Debian-style
[apt](https://salsa.debian.org/apt-team/apt) archive, and will
(through GitLab CI/CD hooks) store them in a
[Git-LFS](https://git-lfs.com/) enabled git repository.

The index files are the "release" index files (`dists/*/*Release*`),
"source" index files (`dists/*/source/*/Sources*`) and "package" index
files (`dists/*/binary-*/*/Packages*`).

For non-Git-LFS backwards compatibility, we also store the files in
one git repository with the (relatively small) release index files.
For size constraints, we are likely to drop these soon though,
focusing on the Git-LFS approach.

We maintain repositories for the following archives:

| Apt archive | Git-LFS Release+Package | Release |
| ----------- | ----------------------- | ------- |
| [Trisquel GNU/Linux](https://trisquel.info/) | https://gitlab.com/debdistutils/dists/trisquel | https://gitlab.com/debdistutils/archives/trisquel/releases |
| [PureOS](https://pureos.net/) | https://gitlab.com/debdistutils/dists/pureos | https://gitlab.com/debdistutils/archives/pureos/releases |
| [Gnuinos](https://www.gnuinos.org/) | https://gitlab.com/debdistutils/dists/pureos | https://gitlab.com/debdistutils/archives/pureos/releases |
| [Ubuntu](https://ubuntu.com/) | https://gitlab.com/debdistutils/dists/ubuntu | https://gitlab.com/debdistutils/archives/ubuntu/releases |
| [Debian](https://www.debian.org/) | https://gitlab.com/debdistutils/dists/debian | https://gitlab.com/debdistutils/archives/debian/releases |
| [Devuan](https://www.devuan.org/) | https://gitlab.com/debdistutils/dists/devuan | https://gitlab.com/debdistutils/archives/devuan/releases |

Our focus are on archives that we hope are compliant with the [Free
System Distribution Guidelines
(GNU-FSDG)](https://www.gnu.org/distros/free-system-distribution-guidelines.html),
but we will support other archives too.

The purpose of this project is to provide infrastructure for our other
projects that increase trust in software published in apt archives,
see [apt-canary](https://gitlab.com/debdistutils/apt-canary) and
[apt-sigstore](https://gitlab.com/debdistutils/apt-sigstore), our
sister project
[debdistcanary](https://gitlab.com/debdistutils/debdistcanary/), and
ultimately combining this with [reproducible
builds](https://reproducible-builds.org/) as in
[debdistreproduce](https://gitlab.com/debdistutils/debdistreproduce/).

## Design (Git-LFS)

The [debdistget-dists](debdistget-dists) script attempts to download
both `Release` metadata files and the (larger) `Packages` and
`Sources` files for a single distribution.

The [ci-debdistget-dists.yml](ci-debdistget-dists.yml) file is a
GitLab CI/CD hook to include from a `.gitlab-ci.yml` file (or through
default CI/CD configuration) that will create and store downloaded
files into the git repository.

To speed up CI/CD jobs, we derive a custom
[container](container-debdistget/Dockerfile) from [Kevin's Trisquel
docker
images](https://gitlab.trisquel.org/kpengboy/docker-brew-trisquel).

## Design (non-Git-LFS)

The [debdistget-release](debdistget-release) script attempts to
download `Release` metadata files for a single distribution.

The [debdistget-packages](debdistget-packages) script attempts to
download both `Release` metadata files and the (much larger)
`Packages` and `Sources` files for a single distribution.

The [ci-debdistget-release.yml](ci-debdistget-release.yml) file is a
GitLab CI/CD hook to include from a `.gitlab-ci.yml` file (or through
default CI/CD configuration) to maintain a git repository with only
the relatively small release files for a distribution.

The [ci-debdistget-packages.yml](ci-debdistget-packages.yml) file is a
GitLab CI/CD hook to include from a `.gitlab-ci.yml` file (or through
default CI/CD configuration) to maintain a git repository with both
release and binary/source packages files for a distribution.

## Setup

To add a new archive, follow these steps replacing `trisquel` with the
short name of the archive you wish to monitor.

- Create a GitLab group (or sub-group) to hold all your archives:
  `dists` -- https://gitlab.com/debdistutils/dists

- Create new GitLab projects `trisquel`in that group: `dists/trisquel`
  -- https://gitlab.com/debdistutils/dists/trisquel

- For the new group (or project), go to Settings -> Access Tokens, and
  create a new token.  For example, use name `debdistget` with no
  expiration date, `Maintainer` role, and `read_repository` and
  `write_repository` rights.  Copy the newly created token to the
  clipboard.

- For the new group (or project), go to Settings -> CI/CD -> Variables
  and add a variable with key `DEBDISTGET_TOKEN` and the value to the
  secret token from the clipboard.  You must select 'Mask variable'.

- Create `pipeline-trisquel.yml` in this project (or a fork), deriving
  it from existing `pipeline-*.yml` files.

- For the new project, go to Settings -> CI/CD -> General pipeline ->
  CI/CD configuration files and set it to
  `https://gitlab.com/debdistutils/debdistget/-/raw/main/pipeline-trisquel.yml`.

- Push empty commit to the new project to trigger a pipeline.

```
git clone git@gitlab.com:debdistutils/dists/trisquel.git
cd trisquel
git switch -c main
git commit --allow-empty -m "Add."
git push -u origin
git lfs install
```

- For the new project, go to Build -> Pipeline Schedules to setup
  scheduled runs.

## Setup (non-Git-LFS)

To add a new apt archive, follow these steps replacing `trisquel` with
the short name of the archive you wish to monitor.

- Create a new GitLab group (or sub-group) for your distribution:
  `trisquel`.

- Create new GitLab projects `releases` and `packages` in that group:
  `trisquel/releases` and `trisquel/packages`.

- For the new group (or projects), go to Settings -> Access Tokens,
  and create a new token.  For example, use name `debdistget` with no
  expiration date, `Maintainer` role, and `read_repository` and
  `write_repository` rights.  Copy the newly created token to the
  clipboard.

- For the new group (or projects), go to Settings -> CI/CD -> Variables
  and add a variable with key `DEBDISTGET_TOKEN` and the value to the
  secret token from the clipboard.  You must select 'Mask variable'.

- Create `ci-trisquel-release.yml` and `ci-trisquel-packages.yml` in
  this project (or a fork), deriving it from existing
  `ci-*-release.yml` and `ci-*-packages.yml` files.

- For the new projects, go to Settings -> CI/CD -> General pipeline ->
  CI/CD configuration files and set it to
  `https://gitlab.com/debdistutils/debdistget/-/raw/main/ci-trisquel-release.yml`
  for the `trisquel/releases` project and
  `https://gitlab.com/debdistutils/debdistget/-/raw/main/ci-trisquel-packages.yml`
  for the `trisquel/packages` project.

- Push empty commit to the new project to trigger a pipeline.

```
git clone git@gitlab.com:debdistutils/archives/trisquel/releases.git
cd releases
git switch -c main
git commit --allow-empty -m "Add."
git push -u origin
```

```
git clone git@gitlab.com:debdistutils/archives/trisquel/packages.git
cd packages
git switch -c main
git commit --allow-empty -m "Add."
git push -u origin
```

- For the new projects, go to Build -> Pipeline Schedules to setup
  scheduled runs.

## License

Any file in this project is free software: you can redistribute it
and/or modify it under the terms of the GNU Affero General Public
License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

See the file [COPYING](COPYING) or
[https://www.gnu.org/licenses/agpl-3.0.en.html] for license text.

## Contact

Please test and [send a
report](https://gitlab.com/debdistutils/debdistget/-/issues) if
something doesn't work.

The author of this project is [Simon
Josefsson](https://blog.josefsson.org/).
